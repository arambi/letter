<?php
namespace Letter\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Letter\Model\Table\LettersTable;
use Manager\TestSuite\CrudTestCase;
use Letter\Collector\LetterCollector;


/**
 * Letter\Model\Table\LettersTable Test Case
 */
class LettersTableTest extends CrudTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.letter.letters',
        'plugin.letter.letters_i18n',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Letters') ? [] : ['className' => 'Letter\Model\Table\LettersTable'];
        $this->Letters = TableRegistry::get('Letters', $config);
        $this->LettersI18n = TableRegistry::get('Letter.LettersI18n');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Letters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
          
      public function testConfig()
      {
        $this->assertCrudDataEdit( 'update', $this->Letters);
        $this->assertCrudDataIndex( 'index', $this->Letters);
      }

      public function testSaveLetters()
      {
        $this->Letters->saveLetters();
      }


      public function testReadLetters()
      {
        $this->Letters->saveLetters();
        
        $letter = LetterCollector::get( 'User.register', [
          'name' => 'Arturo',
          'email' => 'arturo@arambis.com',
          'shop_url' => 'http://www.shop.com',
          'shop_name' => 'Restaurados'
        ]);
      }
}
