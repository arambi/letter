<?php

namespace Letter\Test\Collector;

use Cake\TestSuite\TestCase;
use Letter\Collector\LetterCollector;

class LetterCollectorTest extends TestCase
{

  public $fixtures = [
  ];
  
  public function setUp() 
  {
    parent::setUp();
  }


  public function tearDown() 
  {
    parent::tearDown();
  }

  public function testReplace()
  {
    $text = 'Hemos añadido {points} a tu cuenta de {shop_name}.';
    $vars = [
      'points' => 2,
      'shop_name' => 'Tienda'
    ];

    $result = LetterCollector::replace( $text, $vars);
    $this->assertEquals( 'Hemos añadido 2 a tu cuenta de Tienda.', $result);
  }


}