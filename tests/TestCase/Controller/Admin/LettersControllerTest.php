<?php
namespace Letter\Test\TestCase\Controller\Admin;

use Cake\TestSuite\IntegrationTestCase;
use Letter\Controller\Admin\LettersController;

/**
 * Letter\Controller\Admin\LettersController Test Case
 */
class LettersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.letter.letters'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
