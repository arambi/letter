<?php
namespace Letter\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Letter\Controller\LettersController;

/**
 * Letter\Controller\LettersController Test Case
 */
class LettersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.letter.letters'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
