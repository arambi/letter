<?php
use Cake\Routing\Router;

Router::plugin('Letter', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});
