<?php 
use Section\Action\ActionCollection;
 
use Manager\Navigation\NavigationCollection;
 
use User\Auth\Access;
 




Access::add( 'letters', [
  'name' => 'Emails',
  'options' => [
    'edit' => [
      'name' => 'Edición',
      'nodes' => [
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'add',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Letter',
          'controller' => 'Letters',
          'action' => 'preview',
        ]
      ]
    ]
  ]
]);


NavigationCollection::add([
  'parent' => 'settings',
  'parentName' => 'Configuración',
  'name' => 'Emails',
  'plugin' => 'Letter',
  'controller' => 'Letters',
  'action' => 'index',
  'icon' => 'fa fa-envelope',
]);

