<?php

use Phinx\Migration\AbstractMigration;

class Translations extends AbstractMigration
{
  public function up()
  {
    $letters = $this->table( 'letters_translations', ['id' => false, 'primary_key' => ['id', 'locale']]);
    $letters
      ->addColumn( 'id', 'integer', ['null' => false])
      ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 5])
      ->addColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])
      ->save();  
  }

  public function down()
  {
    $this->dropTable( 'letters_translations');
  }
}
