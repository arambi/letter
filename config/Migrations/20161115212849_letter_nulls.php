<?php

use Phinx\Migration\AbstractMigration;

class LetterNulls extends AbstractMigration
{

  public function change()
  {
    $letters = $this->table( 'letters');
    $letters
      ->changeColumn( 'subject', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->save();
  }
}
