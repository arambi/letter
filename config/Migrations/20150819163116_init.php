<?php

use Phinx\Migration\AbstractMigration;

class Init extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     
    public function change()
    {

    }
    */
   
   public function up()
    {
      $letters = $this->table( 'letters');
      $letters
            ->addColumn( 'site_id', 'integer', ['null' => true, 'default' => null, 'limit' => 4])

            ->addColumn( 'title', 'string', ['null' => false, 'limit' => 255])

            // Nombre computerizado
            ->addColumn( 'cname', 'string', ['null' => false, 'limit' => 255])

            ->addColumn( 'subject', 'string', ['null' => false, 'limit' => 255])

            // Variables a usar en el mail
            ->addColumn( 'variables', 'text', ['default' => NULL, 'null' => true])

            // Campos de texto
            ->addColumn( 'body', 'text', ['default' => NULL, 'null' => true])

            ->addIndex( ['site_id'])
            ->addIndex( ['cname'])
            ->save();

      $letters_i18n = $this->table( 'letters_i18n');
      $letters_i18n
            ->addColumn( 'locale', 'string', ['limit' => 5])
            ->addColumn( 'model', 'string', ['limit' => 64, 'null' => false])
            ->addColumn( 'foreign_key', 'string', ['limit' => 36, 'null' => false])
            ->addColumn( 'field', 'string', ['limit' => 64, 'null' => false])
            ->addColumn( 'content', 'text', ['default' => null, 'null' => true])
            ->addColumn( 'created', 'datetime', array('default' => null))
            ->addColumn( 'modified', 'datetime', array('default' => null))
            ->addIndex( ['locale', 'model', 'foreign_key', 'field'], ['unique' => true])
            ->addIndex( ['locale', 'model', 'foreign_key'], ['unique' => false])
            ->addIndex( ['locale', 'model'], ['unique' => false])
            ->addIndex( ['model', 'foreign_key', 'field'], ['unique' => false])
            ->addIndex( ['model', 'foreign_key'], ['unique' => false])
            ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
      $this->dropTable( 'letters');
      $this->dropTable( 'letters_i18n');
    }
}
