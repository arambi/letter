<?php

use Phinx\Migration\AbstractMigration;

class TranslationsSubject extends AbstractMigration
{
  public function change()
  {
    $letters = $this->table( 'letters_translations');
    $letters
      ->addColumn( 'subject', 'string', ['null' => true, 'default' => null, 'limit' => 255])
      ->removeColumn( 'title')
      ->save(); 
  }
}
