<?php

use Migrations\AbstractMigration;

class AttachmentesNulls extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->table('email_queue')
            ->changeColumn('attachments', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->changeColumn('template_vars', 'text', [
                'default' => null,
                'limit'   => null,
                'null'    => true,
            ])
            ->update();
    }
}
