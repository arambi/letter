<div class="form-group">
  <label ng-help="" tooltip="<?= __d( 'admin', 'Variables que pueden ser usadas dentro del texto. Utilízalas con dos llaves de apertura {}') ?>" class="col-sm-2 control-label ng-scope ng-binding cursor-help"><span class="control-tooltip"><?= __d( 'admin', 'Variables') ?></span></label>
  <div class="col-sm-10">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover dataTables-example">
        <thead>
          <tr>
            <th style="width: 150px"><?= __d( 'admin', 'Nombre') ?></th>
            <th><?= __d( 'admin', 'Descripción') ?></th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="(key, label) in data.content.vars">
            <td style="width: 150px"><span class="badge">{{ key }}</span></td>
            <td>{{ label }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>