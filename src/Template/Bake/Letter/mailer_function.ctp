  public function <%= $name_function %>( $content)
  {
    $this->letter = LetterCollector::get( '<%= $plugin %>.<%= $name_underscore %>', [
      
    ]);

    $this->_setLetterParams();
    $this->_email->setFrom( $content->email);
    $this->_email->setTo( Website::get( 'settings.users_reply_email'));
  }