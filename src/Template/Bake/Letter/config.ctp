LetterCollector::add( '<%= $plugin %>.<%= $name_underscore %>', [
  'title' => '<%= $title %>',
  'subject' => [
    'spa' => '<%= $subject %>'
  ],
  'file' => '<%= $name_underscore %>',
  'vars' => [
    '' => '',
  ]
]);
