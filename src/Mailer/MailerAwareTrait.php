<?php

namespace Letter\Mailer;

use Cake\Core\App;
use Cake\Mailer\Exception\MissingMailerException;
use Letter\Mailer\QueueEmail;

/**
 * Provides functionality for loading mailer classes
 * onto properties of the host object.
 *
 * Example users of this trait are Cake\Controller\Controller and
 * Cake\Console\Shell.
 */
trait MailerAwareTrait
{

    /**
     * Returns a mailer instance.
     *
     * @param string $name Mailer's name.
     * @param \Cake\Mailer\Email|null $email Email instance.
     * @return \Cake\Mailer\Mailer
     * @throws \Cake\Mailer\Exception\MissingMailerException if undefined mailer class.
     */
    public function getMailer($name, QueueEmail $email = null)
    {
        if ($email === null) {
            $email = new QueueEmail();
        }

        $className = App::className($name, 'Mailer', 'Mailer');

        if (empty($className)) {
            throw new MissingMailerException(compact('name'));
        }

        return (new $className($email));
    }
}
