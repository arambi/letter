<?php 

namespace Letter\Mailer;

use Cake\Mailer\Email;

class QueueEmail extends Email
{
  public function getContent( $content = null)
  {
    $rendered = $this->_renderTemplates($content);
    return $rendered ['html'];
  }
}