<?php
namespace Letter\Mailer;

use Cake\Mailer\Mailer;
use Letter\Collector\LetterCollector;
use Letter\Mailer\LetterTrait;
use Website\Lib\Website;

/**
 * Letters mailer.
 */
class LettersMailer extends Mailer
{
  use LetterTrait;

  /**
   * Mailer's name.
   *
   * @var string
   */
  static public $name = 'Letters';

  public function preview( $email, $letter)
  {
    $this->letter = LetterCollector::get( $letter->cname, array_combine( array_keys( $letter->vars), array_values( $letter->vars)));

    $this->_setLetterParams();

    $this->_email->from( Website::get( 'settings.users_reply_email'), Website::get( 'title'));
    $this->_email->to( $email);
  }

  
}
