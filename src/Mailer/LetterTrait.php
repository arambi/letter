<?php

namespace Letter\Mailer;

use Website\Lib\Website;
use Cake\Core\Configure;

/**
 * Conjunto de utilidades para las instancias de Mailer que usen Letter
 */

trait LetterTrait
{
    /**
     * La entidad letter que debe ser seteada
     * @var Letter\Model\Entity|Letter
     */
    public $letter;

    // Setea los parámetros básicos para el envio de un mail con Letter
    protected function _setLetterParams()
    {
        $this->_email
            ->subject($this->letter->subject);

        $this->_email
            ->transport('default')
            ->template('Letter.view', Website::get('theme') . '.default')
            ->set('body', $this->letter->body)
            ->emailFormat('html');

        if (Configure::read('App.emailReturnPath')) {
            $this->_email->returnPath(Configure::read('App.emailReturnPath'));
        }
    }

    /**
     * Devuelve un elemento renderizado
     * Sirve para setear variables que contengan el contenido de este elemento
     * y que quiera ser usado en el mail como variable
     * 
     * @param  string   $element  La ruta del elemento
     * @param  array    $vars     Variables a usar en el elemento
     * @return string           El elemento renderizado
     */
    public function element($element, $vars = [])
    {
        $className = $this->viewBuilder()->className();
        return (new $className)->element($element, $vars);
    }
}
