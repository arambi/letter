<?php

namespace Letter\Mailer;

use Cake\I18n\I18n;
use Letter\EmailQueue;
use Cake\Mailer\Mailer;
use Cake\Routing\Router;
use Letter\Mailer\QueueEmail;
use Letter\Mailer\LetterTrait;
use Pelago\Emogrifier\CssInliner;
use Letter\Collector\LetterCollector;
use Pelago\Emogrifier\HtmlProcessor\HtmlPruner;
use Cake\Mailer\Exception\MissingActionException;
use Pelago\Emogrifier\HtmlProcessor\CssToAttributeConverter;

/**
 * Letters mailer.
 */
class QueueMailer extends Mailer
{
    private $currentLocale = false;


    public function __construct(QueueEmail $email = null)
    {

        if ($email === null) {
            $email = new QueueEmail();
        }

        $this->_email = $email;
        $this->_clonedEmail = clone $email;
    }

    public function setLocale($locale)
    {
        if (!$this->currentLocale) {
            $this->currentLocale = I18n::getLocale();
        }

        I18n::setLocale($locale);
        Router::reload();
    }

    public function send($action, $args = [], $headers = [], $locale = false)
    {
        if (!empty($locale)) {
            $this->setLocale($locale);
        }

        if (!method_exists($this, $action)) {
            throw new MissingActionException([
                'mailer' => $this->getName() . 'Mailer',
                'action' => $action,
            ]);
        }

        $this->_email->setHeaders($headers);
        if (!$this->_email->viewBuilder()->getTemplate()) {
            $this->_email->viewBuilder()->setTemplate($action);
        }

        call_user_func_array([$this, $action], $args);

        $from = $this->_email->getFrom();
        $options = [
            'subject' => $this->_email->getOriginalSubject(),
            'from_name' => current($from),
            'from_email' => key($from),
            'format' => 'html',
            'layout' => 'Letter.queue',
            'template' => 'Letter.queue',
        ];

        $content = $this->_email->getContent();
        $content = $this->emogrifier($content);
        EmailQueue::enqueue($this->_email->getTo(), ['body' => $content], $options);

        if ($this->currentLocale) {
            I18n::setLocale($this->currentLocale);
            Router::reload();
        }

        $this->reset();
    }

    /**
     * Embebe los estilos CSS del fichero /css/email.css dentro del mail
     *
     * @param string $content
     * @return string
     */
    public function emogrifier($content)
    {
        $file_css = ROOT . DS . 'webroot' . DS . 'css' . DS . 'email.css';

        if (!file_exists($file_css)) {
            return $content;
        }

        $css = file_get_contents($file_css);
        $css = str_replace('@charset "UTF-8";', '', $css);
        // $emogrifier = new \Pelago\Emogrifier();
        // $emogrifier->setHtml( $content);
        // $emogrifier->setCss($css);
        // $mergedHtml = $emogrifier->emogrify();
        // $bodyContent = $emogrifier->emogrifyBodyContent();

        $cssInliner = CssInliner::fromHtml($content)->inlineCss($css);
        $domDocument = $cssInliner->getDomDocument();
        HtmlPruner::fromDomDocument($domDocument)->removeElementsWithDisplayNone()
            ->removeRedundantClassesAfterCssInlined($cssInliner);
        $mergedHtml = CssToAttributeConverter::fromDomDocument($domDocument)
            ->convertCssToVisualAttributes()->render();

        return $mergedHtml;
    }
}
