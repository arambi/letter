<?php
namespace Letter\Controller\Admin;

use Manager\Controller\CrudControllerTrait;
use Letter\Controller\AppController;
use Cake\Mailer\MailerAwareTrait;

/**
 * Letters Controller
 *
 * @property \Letter\Model\Table\LettersTable $Letters
 */
class LettersController extends AppController
{
  use CrudControllerTrait;
  use MailerAwareTrait;

  public function _index()
  {
    $this->Letters->saveLetters();
  }

  public function _update()
  {
    
  }

  public function preview( $id)
  {
    $this->CrudTool->serializeAction( false);

    if( !filter_var( $this->request->data( 'email'), FILTER_VALIDATE_EMAIL)) 
    {
      $this->set([
        'text' => 'El email no es válido',
        'type' => 'error'
      ]);
    }
    else
    {
      $letter = $this->Table->get( $id);

      $this->getMailer( 'Letter.Letters')->send( 'preview', [
          $this->request->data( 'email'),
          $letter,
      ]);

      $this->set([
        'text' => 'El email ha sido enviado',
        'type' => 'success'
      ]);
    }
  
    $this->set( '_serialize', true);
  }
}
