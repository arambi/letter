<?php

namespace Letter\Collector;

use Letter\Collector\Sheet;
use Cake\ORM\TableRegistry;

/**
 * Class Estática encargada de recolectar los emails de la aplicación
 * 
 */

class LetterCollector
{
  protected static $_items = [];

  public static function add( $cname, $data)
  {
    // Toma el directorio desde donde se ha llamado
    $backtrace = debug_backtrace();
    $dir = dirname( $backtrace [0]['file']) .DS. 'Letter' .DS;
    $data ['dir'] = $dir;
    static::$_items [$cname] = new Sheet( $cname, $data);
  }

  public static function build( $langs)
  {
    $out = [];

    foreach( static::$_items as $cname => $item)
    {
      $out [$cname] = $item->build( $langs);
    }

    return $out;
  }

  public static function sheet( $cname)
  {
    if( isset( static::$_items [$cname]))
    {
      return static::$_items [$cname];
    }

    return false;
  }

  public static function get( $cname, $vars = [])
  {
    $letter = TableRegistry::get( 'Letter.Letters')->find()
      ->where( ['Letters.cname' => $cname])
      ->first();
    
    if( !$letter)
    {
      return false;
    }
    
    $letter->subject = self::replace( $letter->subject, $vars);
    $letter->body = self::replace( $letter->body, $vars);

    return $letter;
  }

  public static function replace( $string, $vars)
  {
    preg_match_all( '/{(.*?)}/', $string, $match);

    if( !empty( $match))
    {
      foreach( $match [1] as $key => $var)
      {
        $replace = isset( $vars [$var]) ? $vars [$var] : '';
        $string = str_replace( $match [0][$key], $replace, $string);
      }
    }

    preg_match_all( '/\[(.+)\]/', $string, $match); 

    if( !empty( $match))
    {
      foreach( $match [1] as $key => $found)
      {
        if( strpos( $found, '|') !== false)
        {
          list( $title, $url) = explode( '|', $found);
        }
        else
        {
          $title = $url = $found;
        }

        $replace = '<a href="'. $url .'">'. $title .'</a>';
        $string = str_replace( $match [0][$key], $replace, $string);
      }
    }

    return $string;
  }
}