<?php 

namespace Letter\Collector;


/**
 * Class que instancia los emails usados en la aplicación
 */

class Sheet
{
  public $subject;

  public $vars = [];

  public $file;

  public $dir;

  public $title;

  public $body;

  public $langs = [];

  public $cname;

  public function __construct( $cname, $data)
  {
    $this->cname = $cname;

    foreach( $data as $key => $value)
    {
      $this->$key = $value;
    }
  }

  public function build( $langs)
  {
    $this->langs = $langs;

    $subject = $this->subject();
    $body = $this->body();

    $out = [
      'cname' => $this->cname,
      'title' => $this->title,
    ];

    foreach( $langs as $lang)
    {
      $out ['_translations'][$lang]['subject'] = $subject [$lang];
      $out ['_translations'][$lang]['body'] = $body [$lang];
    }

    return $out;
  }

  public function subject()
  {
    $out = [];

    if( empty( $this->langs))
    {
      return current( $this->subject);
    }

    foreach( $this->langs as $lang)
    {
      if( isset( $this->subject [$lang]))
      {
        $out [$lang] = $this->subject [$lang];
      }
      else
      {
        $out [$lang] = current( $this->subject);
      }
    }

    return $out;
  }

  public function body()
  {
    $out = [];

    foreach( $this->langs as $lang)
    {
      $file = $this->dir . $lang .DS. $this->file .'.ctp';

      if( file_exists( $file))
      {
        $out [$lang] = nl2br( file_get_contents( $file));
      }
      else
      {
        $out [$lang] = '';
      }
    }

    return $out;
  }



}