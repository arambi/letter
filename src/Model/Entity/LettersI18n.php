<?php
namespace Letter\Model\Entity;

use Cake\ORM\Entity;

/**
 * LettersI18n Entity.
 */
class LettersI18n extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
