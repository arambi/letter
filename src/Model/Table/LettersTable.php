<?php
namespace Letter\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Letter\Model\Entity\Letter;
use Letter\Collector\LetterCollector;
use I18n\Lib\Lang;
use Cake\Core\Configure;

/**
 * Letters Model
 */
class LettersTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
      parent::initialize($config);

      $this->table('letters');
      $this->displayField('title');
      $this->primaryKey('id');
      
      // Behaviors
      $this->addBehavior( 'Manager.Crudable');
      $this->addBehavior( 'Cofree.Saltable');
      $this->addBehavior( Configure::read( 'I18n.behavior'), [
        'fields' => ['subject', 'body'],
        'translationTable' => 'Letter.LettersI18n'
      ]);

      // CRUD Config
      //Escribe aquí las asociaciones vinculadas con el model a la hora de editar (si las hubiera)
      // $this->crud->associations([]);

      $this->crud
        ->addFields([
          'variables' => [
            'type' => 'string',
            'template' => 'Letter.fields.variables'
          ],
          'title' => [
            'label' => __d( 'admin', 'Título'),
            'help' => __d( 'admin', 'Título interno para este email')
          ],
          'subject' => [
            'label' => __d( 'admin', 'Tema'),
            'help' => __d( 'admin', 'El tema del email')
          ],
          'body' => [
            'label' => __d( 'admin', 'Cuerpo de texto'),
            'help' => __d( 'admin', 'El texto que irá en el email')
          ],
          'preview' => [
            'type' => 'string',
            'template' => 'Letter.fields/preview'
          ],
        ])
        ->addIndex( 'index', [
          'fields' => [
            'title',
            'subject',
            'cname',
          ],
          'actionButtons' => [],
          'saveButton' => false,
        ])
        ->setName( [
          'singular' => __d( 'admin', 'Emails'),
          'plural' => __d( 'admin', 'Emails'),
        ])
        ->addView( 'create', [
          'columns' => [
            [
              'cols' => 8,
              'box' => [
                [
                  'elements' => [ 
                    'preview',
                    'title',
                    'variables',
                    'subject',
                    'body',
                  ]
                ]
              ]
            ]
          ],
          'actionButtons' => ['index']
        ], ['update'])
    ;
    
  } 


/**
 * Acciones de modificación de resultados
 *  
 * @param  Event $event
 * @param  Query  $query
 * @return void
 */
  public function beforeFind( $event, Query $query)
  {
    $query->formatResults(function ($results) {
      return $this->_rowMapper($results);
    }, $query::PREPEND);
  }

/**
 * Coloca la información de Sheet
 *  
 * @param Collection|Entity $results 
 * @return void
 */
  protected function _rowMapper( $results) 
  {
    return $results->map(function ($letter) {
      if ($letter === null || !is_object( $letter)) {
        return $letter;
      }

      // Setea la información del letters
      $sheet = LetterCollector::sheet( $letter->cname);

      if ($sheet) {
        $letter->set( 'vars', LetterCollector::sheet( $letter->cname)->vars);
      }

      return $letter;
    });
  }



  public function saveLetters()
  {
    $letters = LetterCollector::build( Lang::combine( 'iso3', 'iso3'));
    
    foreach( $letters as $letter)
    {
      if( !$this->exists([$this->alias() .'.cname' => $letter ['cname']]))
      {
        $entity = $this->getNewEntity( $letter);
        $this->saveContent( $entity);
      }
    }
  }
}
