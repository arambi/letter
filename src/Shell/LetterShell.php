<?php
namespace Letter\Shell;

use Cake\Console\Shell;
use Cofree\Traits\TemplateTrait;
use Cake\Utility\Inflector;
use Cake\Core\Plugin;
use Cake\Core\App;
use Cake\Filesystem\Folder;

/**
 * Letter shell command.
 */
class LetterShell extends Shell
{
  use TemplateTrait;
  
  public $tasks = [
    'Bake.BakeTemplate',
  ];

  /**
   * Manage the available sub-commands along with their arguments and help
   *
   * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
   *
   * @return \Cake\Console\ConsoleOptionParser
   */
  public function getOptionParser()
  {
    $parser = parent::getOptionParser();

    return $parser;
  }

  /**
   * main() method.
   *
   * @return bool|int|null Success or error code.
   */
  public function main()
  {
    $this->out($this->OptionParser->help());
  }

  public function create()
  {
    $plugin = $this->in( 'Indica el plugin donde irá el letter');
    $plugin = Inflector::camelize( $plugin);

    if( !Plugin::loaded( $plugin))
    {
      $this->error( vsprintf( "El plugin %s no está disponible en la aplicación", [$plugin]));
    }

    $name = $this->in( 'Indica el nombre computerizado de letter');
    $name = Inflector::camelize( $name);
    $title = $this->in( 'Indica un nombre');
    $subject = $this->in( 'Indica un subject');
    $name_underscore =  Inflector::underscore( $name);
    $name_function = lcfirst( $name); 
    $params = compact( 'name', 'plugin', 'title', 'subject', 'name_underscore', 'name_function');

    $mailer = $this->__setMailer( $params);
    $this->__writeConfig( $params);
    $this->__createBody( $params);
  }
  

  private function __setMailer( $params)
  {
    $plugin = $params ['plugin'];
    $path = Plugin::path( $plugin) . 'src' .DS. 'Mailer';
    $tree = (new Folder( $path))->tree();

    if( isset( $tree [1]) && is_array( $tree [1]))
    {
      foreach( $tree [1] as $key => $file)
      {
        $file = $this->__getMailerName( $path, $file);
        $this->out( ($key + 1) . '. '. $file);
      }

      $key_file = $this->in( 'Indica un mailer o 0 para nuevo');
      
      if( $key_file == '0')
      {
        $name = $this->__createMailer( $plugin);
      }
      else
      {
        if( !isset( $tree [1][$key_file - 1]))
        {
          $this->warn( 'El mailer seleccionado no existe');
          return $this->__getMailer( $plugin);
        }

        $name = $this->__getMailerName( $path, $tree [1][$key_file - 1]);
      }
    }
    else
    {
      $name = $this->__createMailer( $plugin);
    }

    $filepath = Plugin::path( $plugin) . 'src' .DS. 'Mailer' .DS. $name .'Mailer.php';
    $contents = file_get_contents( $filepath);
    $this->BakeTemplate->set( $params);
    $template = $this->BakeTemplate->generate( 'Letter.Letter/mailer_function');

    $pos = strrpos( $contents, '}') - 1;
    $contents = substr_replace( $contents, "\n\n\n\n" . $template, $pos, 0);
    file_put_contents( $filepath, $contents);
  }

  private function __getMailerName( $path, $file)
  {
    $file = str_replace( $path .DS, '', $file);
    $file = str_replace( 'Mailer.php', '', $file);
    return $file;
  }

  private function __createMailer( $plugin)
  {
    $name = $this->in( 'Indica el nombre');
    $this->dispatchShell( 'bake mailer '. $plugin . '.'. $name);
    $filepath = Plugin::path( $plugin) . 'src' .DS. 'Mailer' .DS. $name .'Mailer.php';
    $contents = file_get_contents( $filepath);
    $ref = 'use Cake\Mailer\Mailer;';
    $pos = strpos( $contents, $ref) + strlen( $ref) + 1;
    $template = $this->BakeTemplate->generate( 'Letter.Letter/mailer_namespaces');
    $contents = substr_replace( $contents, $template, $pos, 0);
    file_put_contents( $filepath, $contents);
    return $name;
  }

/**
 * Escribe la configuración de letter.php
 *
 * @param array $params
 * @return void
 */
  private function __writeConfig( array $params)
  {
    extract( $params);
    $this->BakeTemplate->set( $params);
    $contents = $this->BakeTemplate->generate( 'Letter.Letter/config');
    $file_content = $this->__getLetterConfig( $plugin);
    $file_content .= "\n\n\n\n" . $contents;
    file_put_contents( $this->__configPath( $plugin), $file_content);
  }

  /**
   * Escribe el fichero body
   *
   * @param array $params
   * @return void
   */
  private function __createBody( array $params)
  {
    $path = Plugin::path( $params ['plugin']) . 'config' .DS. 'Letter' .DS. 'spa' .DS. $params ['name_underscore'] . '.ctp';
    $this->createFile( $path, '');
  }

  public function in( $prompt, $options = null, $default = null)
  {
    return parent::in( $prompt, $options, $default);
  }

  /**
   * Devuelve el path del fichero de configuración
   *
   * @param string $plugin
   * @return string
   */
  private function __configPath( $plugin)
  {
    return Plugin::path( $plugin) . 'config' .DS. 'letters.php';
  }

  /**
   * Devuelve el contenido del fichero de configuración
   *
   * @param string $plugin
   * @return string
   */
  private function __getLetterConfig( $plugin)
  {
    $path = $this->__configPath( $plugin);

    if( !file_exists( $path))
    {
      $content = "<?php \n\n use Letter\Collector\LetterCollector; \n\n";
      $this->createFile( $path, $content);
    }
    else
    {
      $content = file_get_contents( $path);
    }

    return $content;
  }
}
